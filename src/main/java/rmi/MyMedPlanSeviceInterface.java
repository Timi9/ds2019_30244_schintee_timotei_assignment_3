package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.LocalDate;

public interface MyMedPlanSeviceInterface extends Remote {
    Integer getIncreaseNumber(Integer number) throws RemoteException;

    MedicationPlan getDailyMedicationPlan() throws RemoteException;
}
