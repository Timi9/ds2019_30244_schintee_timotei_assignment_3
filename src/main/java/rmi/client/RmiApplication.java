package rmi.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import rmi.MyMedPlanSeviceInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

@SpringBootApplication
public class RmiApplication {

	public static void main(String[] args)  throws RemoteException, NotBoundException {
		SpringApplication.run(RmiApplication.class, args);
		//        if (System.getSecurityManager()==null){
//            System.setSecurityManager(new SecurityManager());
//        }


	}


}
