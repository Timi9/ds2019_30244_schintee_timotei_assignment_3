package rmi.client;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rmi.MedicationPlan;
import rmi.MyMedPlanSeviceInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


@RequestMapping("medicationPlan")
@RestController
@CrossOrigin
public class MedPlanController {
    private MyMedPlanSeviceInterface stub;

    public MedPlanController() throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("127.0.0.1");
        stub = (MyMedPlanSeviceInterface) registry.lookup("MyMedPlanSeviceObject");

        System.out.println("Increased number: " + stub.getIncreaseNumber(3));
    }

    @GetMapping("getDailyMedicationPlan")
    public MedicationPlan getDailyMedPlan() throws RemoteException {
        System.out.println("NEED MEDPLAN");
        return stub.getDailyMedicationPlan();
    }
}
