package rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class MyServer {

    public static void main(String[] args){
//        if (System.getSecurityManager() == null){
//            System.setSecurityManager(new SecurityManager());
//        }

        MyMedPlanSeviceInterface myRemoteObject = new MyMedPlanSeviceObject();
        try{
            MyMedPlanSeviceInterface stub = (MyMedPlanSeviceInterface) UnicastRemoteObject.exportObject(myRemoteObject,0);
            Registry registry = LocateRegistry.createRegistry(1099);

            registry.bind("MyMedPlanSeviceObject",stub);
            System.out.println("bound 'MyMedPlanSeviceObject'");
        }catch (Throwable cause){
            System.err.println("couldn't bind 'MyMedPlanSeviceInterface' cause"+cause.getMessage());
        }
    }
}
