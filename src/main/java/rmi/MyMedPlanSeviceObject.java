package rmi;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MyMedPlanSeviceObject implements MyMedPlanSeviceInterface {
    private static int k =0;
    @Override
    public Integer getIncreaseNumber(Integer number) throws RemoteException {
        return number +1;
    }

    private MedicationPlan getPlanOne(){
        Medication m1 = new Medication("ibuprofen", LocalTime.of(1,0,0),LocalTime.of(3,0,0));
        Medication m2 = new Medication("adagin", LocalTime.of(3,0,0),LocalTime.of(5,0,0));

        List<Medication> medicationList = new ArrayList<>();
        medicationList.add(m1);
        medicationList.add(m2);

        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setMedications(medicationList);
        medicationPlan.setIntakeDate(LocalDate.of(2019,11,10));

        return medicationPlan;
    }

    private MedicationPlan getPlanTwo(){
        Medication m1 = new Medication("paracetamol", LocalTime.of(0,0,0),LocalTime.of(3,0,0));
        Medication m2 = new Medication("tusin", LocalTime.of(1,0,0),LocalTime.of(5,0,0));

        List<Medication> medicationList = new ArrayList<>();
        medicationList.add(m1);
        medicationList.add(m2);

        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setMedications(medicationList);
        medicationPlan.setIntakeDate(LocalDate.of(2019,11,10));

        return medicationPlan;
    }

    @Override
    public MedicationPlan getDailyMedicationPlan() throws RemoteException {
        if (k==0){
            k=1;
            return getPlanOne();
        }
        else
        {
            k=0;
            return getPlanTwo();
        }
    }
}
