import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";

export class Medication {
  name:String;
  intakeIntervalStart:Date;
  intakeIntervalEnd:Date;
}

export class MedicationPlan {
  medications: Medication[];


  constructor(medications:Medication[]) {
    this.medications= medications;
  }
}
@Injectable({
  providedIn: 'root'
})
export class MedPlanService {

  constructor(private http: HttpClient) {
  }


  getMedications(): Observable<MedicationPlan> {
    return this.http.get<MedicationPlan>( 'http://localhost:8080/medicationPlan/getDailyMedicationPlan');
  }

  takeMedication(): Observable<MedicationPlan> {
      return this.http.get<MedicationPlan>( 'http://localhost:8080/medicationPlan/takeMedication');
    }

}
